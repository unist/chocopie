package com.example.core.today;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TimeLine extends Activity {
    /** Called when the activity is first created. */
	private TextView mDateDisplay;
    private Button mPickDate;
    Context context = this;
    private int mYear;
    private int mMonth;
    private int mDay;
    private String year;
    private String month;
    private String day;
    private String YTD_Cur;
    private ArrayList<String> arraylist = new ArrayList<String>();
    static final int DATE_DIALOG_ID = 0;

    private Button yesterday;
    private Button tomorrow;
    private String date;
    Date mdate;
    
    static int count =0;
    private ArrayAdapter<String> adapter;
    private ListView list;

    Calendar calendar = new GregorianCalendar(Locale.KOREA);
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline);
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "THEjunggt120.otf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("PRISM");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_HOME | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
        if(android.os.Build.VERSION.SDK_INT > 9) {
 
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

        }
        // capture our View elements
        yesterday = (Button) findViewById(R.id.yesterday);
        tomorrow = (Button) findViewById(R.id.tomorrow);
        
        mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
        mPickDate = (Button) findViewById(R.id.pickDate);
        list = (ListView) findViewById(R.id.list);

        tomorrow.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				calendar.add(Calendar.DATE, 1);
		        year = String.valueOf(calendar.get(Calendar.YEAR));
		        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
		        day = String.valueOf(calendar.get(Calendar.DATE));
		        mYear   =	calendar.get(Calendar.YEAR);
		        mMonth  =	calendar.get(Calendar.MONTH)+1;
		        mDay 	=	calendar.get(Calendar.DATE);
		        if(month.length()<2)
		        	month = "0" + month;
		        if(day.length()<2)
		        	day = "0" + day;
		        date = year+month+day;
		        if(date.compareTo(YTD_Cur) > 0)
		        		Toast.makeText(context, "기록 정보가 없습니다.", 500).show();
		        else{

		        mDateDisplay.setText(date);
		        remove();
		        TimeLineMaker(date);
		        }

			}
        	
        });
        yesterday.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				calendar.add(Calendar.DATE, -1);
				year = String.valueOf(calendar.get(Calendar.YEAR));
		        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
		        day = String.valueOf(calendar.get(Calendar.DATE));
		        mYear   =	calendar.get(Calendar.YEAR);
		        mMonth  =	calendar.get(Calendar.MONTH)+1;
		        mDay 	=	calendar.get(Calendar.DATE);
	        if(month.length()<2)
		        	month = "0" + month;
		        if(day.length()<2)
		        	day = "0" + day;
		        date = year+month+day;
		        mDateDisplay.setText(date);
		        remove();
		        TimeLineMaker(date);

			}
        	
        });
        // display the current date
        
        // add a click listener to the button
        mPickDate.setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
			public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        calendar.add(Calendar.DATE, -1);
        year = String.valueOf(calendar.get(Calendar.YEAR));
        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
        day = String.valueOf(calendar.get(Calendar.DATE));
        mYear   =	calendar.get(Calendar.YEAR);
        mMonth  =	calendar.get(Calendar.MONTH)+1;
        mDay 	=	calendar.get(Calendar.DATE);

        
        
        if(month.length()<2)
        	month = "0" + month;
        if(day.length()<2)
        	day = "0" + day;
        date = year+month+day;
        YTD_Cur = date;
        Toast.makeText(context, YTD_Cur, 500).show();
        mDateDisplay.setText(date);
        adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, arraylist);
		list.setAdapter(adapter);       
     // capture our View elements
 
        // add a click listener to the button

       

        // display the current date

    }
   
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_DIALOG_ID:
            return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth-1, mDay);
        }
        return null;
    }
   
    // updates the date we display in the TextView
    private void updateDateDisplay() {
    	if(month.length()<2)
    		month = "0" + month;
    	if(day.length()<2)
    		day = "0" + day;
        date = year+month+day;
        if(date.compareTo(YTD_Cur) > 0){
    		Toast.makeText(context, "기록 정보가 없습니다.", 500).show();
    		calendar.add(Calendar.DATE, -1);
        }
        else{
        	mDateDisplay.setText(
                    new StringBuilder()
                            // Month is 0 based so add 1
                    		.append(year)
                            .append(month)
                            .append(day)
                            );
        	calendar.set(mYear, mMonth, mDay);


        	remove();
        	TimeLineMaker(date);
        }

    }
   
    // updates the time we display in the TextView
   private void remove(){
	   if(!arraylist.isEmpty())
	       	for(int i =0; i < arraylist.size() ; i++)
	       		arraylist.removeAll(arraylist);
	   adapter.notifyDataSetChanged();
   }
    // the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int sub_year,
                                      int monthOfYear, int dayOfMonth) {
                    mYear = sub_year;
                    year = String.valueOf(sub_year);
                    mMonth = monthOfYear;
                    month = String.valueOf(monthOfYear+1);
                    mDay = dayOfMonth;
                    day = String.valueOf(dayOfMonth);
                    
                    updateDateDisplay();
                }
            };
           
         // the callback received when the user "sets" the time in the dialog

     private ArrayList<String> TimeLineMaker(String date){
    	 //new TimeLineThread(date).execute(null,null,null);
 //   	int []time;
 //   	int []time2;
    	String key = "";
    	String news = "";
    	String url_info = "";
        String line;
 		String page ="";
 		String group1 = "http://kr.core.today/json2/?n=50&sl=1";
 		String time1 = "&d=";
        String sample = group1 + time1 + date;
        
         URL url;
         HttpURLConnection urlConnection;
         BufferedReader bufreader;
         try {

 		url = new URL(sample);
 		
 		urlConnection = (HttpURLConnection) url.openConnection();

 		bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
 		while((line=bufreader.readLine())!=null){
 			page+=line;
 		}

 		JSONObject test;
 		JSONArray alljson = new JSONArray(page);
 		ArrayList<Integer> arr = new ArrayList<Integer>();
 		for(int i =0 ; i<24 ; i++)
 			arr.add(0);
 		JSONArray time_frequency;
 			try{	
 				for(int k =0 ; k<50 ; k++){
 		
 					test=new JSONObject(alljson.getString(k));
 					
 					time_frequency = new JSONArray(test.getString("t"));
 					
 					for(int index =0 ; index < 24; index++){
 						int frequency = (int)time_frequency.getDouble(index);
 					
 						if(arr.get(index) < frequency)
 							arr.set(index, frequency);
 					}
 				}
 			}
 			catch(Exception e){
 				Toast.makeText(context, "error", 1000).show();
 			}
 		for(int i =0; i < 24 ; i++){
 			test = new JSONObject(alljson.getString(arr.get(i)));
 			key = test.getString("nm");
 			arraylist.add(key);
 		}

 /*
 		for(int i =0 ; i<10; i++){
 			test= new JSONObject(alljson.getString(i));
 			key = test.getString("k");
 			arraylist.add(key);
 		}
 		*/
 		adapter.notifyDataSetChanged();
 		urlConnection.disconnect();
 		
 		}
 		catch(Exception e){
 			remove();
 			arraylist.add("기사가 없습니다");
 		}
         return arraylist;
     }  
     public class NewsViewHolder{
     	TextView tt;
     	TextView bt;
     	TextView ct;
     	boolean isOpen;
     }
     
     @Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		if(android.R.id.home == item.getItemId()){
 			finish();
 		}
 		return super.onOptionsItemSelected(item);
 	}
}