package com.example.core.today;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
//<<<<<<< HEAD
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
//<<<<<<< HEAD
import android.widget.Button;
import android.widget.ImageView;
//=======
//>>>>>>> 87022ddcff81666b2257bfde4746165b8d3955de
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public final class TestFragment extends Fragment implements OnItemClickListener{
    private static final String KEY_CONTENT = "TestFragment:Content";
	private static final int REQUEST_CODE_ANOTHER = 000;
    private static String _content;
    int click_focus = -1; 
    int itemcount=0;

    private Bitmap bmImg;
    Context context = null;
    Animation scaleUpAnimation;
    Animation scaleDownAnimation;
    JsonNewsAdapter adapter;
    static ListView plv = null;
    String u="u";
    private String title;
    
    
    
    public static TestFragment newInstance(String title) {
        TestFragment fragment = new TestFragment();
        Bundle args = new Bundle();
        args.putString("someTitle", title);
        fragment.setArguments(args);
        return fragment;
    }

    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
        	title = getArguments().getString("someTitle");
       // }
        
	    
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    	context = container.getContext(); // get Context

    	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    	plv = (ListView) LayoutInflater.from(context).inflate(
    			R.layout.layout_listview_in_viewpager, container, false);

    	String line;
    	String page ="k";
    	String keyword="a",article="b",image="i", event1="c",event2="d",event3="e";

    	final String group = ("http://kr.core.today/json2/?n=20&sl=1&d="+title);

    	JSONObject test;
    	BufferedReader bufreader = null;
    	HttpURLConnection urlConnection = null;
    	JSONArray alljson;
    	URL url1 = null;
    	ArrayList<JsonNewsItem> m_orders = new ArrayList<JsonNewsItem>();
    	page = "";
    	int count =0;
    	
    	try {

    		String sample = group;
    		url1 = new URL(sample);
    		urlConnection = (HttpURLConnection) url1.openConnection();

    		bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));

    		while((line=bufreader.readLine())!=null){
    			page+=line;
    		}

    		alljson = new JSONArray(page);

    		for(int k =0; k < 10 ; count++){

    			try{
    				test=new JSONObject(alljson.getString(count));

    				JSONArray keyw = new JSONArray(test.getString("k"));
    				if(keyw.getString(0) != "[]" && test.getString("nm") != "[]"){

    						JSONArray arti = new JSONArray(test.getString("nm"));
    						keyword=test.getString("k");
    						article=arti.getString(0);
    //article=test.getString("nm");

    						image = test.getString("i");
    						m_orders.add(new JsonNewsItem(keyword,article, image, event1,event2,event3,bmImg));
    						k++;
    				}

    			}
    			catch(Exception e){

    			}
    		}

    	}
    	catch(Exception e){
    //Toast.makeText(context, "Fail to parse JSON", 1000).show();
    	}
    /////////////////////////////////////////////////////////////////////////////////////////////parse JSON file

    	urlConnection.disconnect();
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    ///////////////////////////////////////////////////////////////////////////////add NewsItem to ArrayList

    	adapter = new JsonNewsAdapter(context, R.layout.listitem , m_orders);
    	plv.setAdapter(adapter);
    	plv.setOnItemClickListener(this);
    	
    //plv.setOnRefreshListener(TestFragment.this); /// set Refresh listener


    	LinearLayout layout = new LinearLayout(getActivity());
    	layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
    	layout.setGravity(Gravity.CENTER);
    	layout.addView(plv);
    /////////////////////////////////////////////////////////////////////////////inflate layout and listview

    	return layout;

    }
    
    	public class NewsViewHolder{
    		TextView keyword;
    		TextView article;
    		TextView event1;
    		TextView event2;
    		TextView event3;
    		ImageView image;
    		TextView detail;
    		boolean isOpen;
    	}
    
    /////////////////////////////////////////////////////////////////////////////////////////
    	public class JsonNewsAdapter extends ArrayAdapter<JsonNewsItem> {
    		private ArrayList<JsonNewsItem> items;
    
 	  public JsonNewsAdapter(Context context, int textViewResourceId, ArrayList<JsonNewsItem> items) {
 		  super(context, textViewResourceId, items);
 		  this.items = items;
 	  }

 	  @Override
 	  public View getView(int position, View convertView, ViewGroup parent) {    
 		  int count;
 		  LayoutParams lp = null;
 		  View v = convertView;
 		  NewsViewHolder holder = null;
 		  int lastvisible = plv.getLastVisiblePosition();
 		  if (v == null) {      
 			  LayoutInflater vi =(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
    	v = vi.inflate(R.layout.listitem, null);
    	holder = new NewsViewHolder();
    	holder.keyword= (TextView) v.findViewById(R.id.dataItem01);
    	holder.article= (TextView) v.findViewById(R.id.dataItem02);
    	holder.event1 = (TextView) v.findViewById(R.id.event1);
    	holder.event2 = (TextView) v.findViewById(R.id.event2);
    	holder.event3 = (TextView) v.findViewById(R.id.event3);
    	holder.image = (ImageView) v.findViewById(R.id.news_image);
    	holder.detail = (TextView) v.findViewById(R.id.detail);

    	holder.isOpen=false;

 		  }
 		  else
 		  {
 			  holder=(NewsViewHolder)v.getTag();
 		  }

 		  JsonNewsItem p = items.get(position);     

 		  v.setTag(holder);
 		  View toolbar = v.findViewById(R.id.toolbar);
 		  lp=(LayoutParams) toolbar.getLayoutParams();
 	  if(holder.isOpen){
 		  toolbar.setVisibility(View.GONE);
 		  lp.bottomMargin=0- v.getHeight();
 		  v.requestLayout();
 	  }

 	  if(p.isOpen){
 		  toolbar.setVisibility(View.VISIBLE);
 		  lp.bottomMargin=0;
 		  v.requestLayout();
 	  }


 	  if(p!=null){
 		  holder.keyword.setText(p.getTitle());
 		  holder.article.setText(p.getLink());
 		  holder.event1.setText(p.getEvent1());
 		  holder.event2.setText(p.getEvent2());
 		  holder.event3.setText(p.getEvent3());
 		  holder.detail.setText(p.getDetail());
 		  holder.isOpen=p.getisOpen();
 	  }
 	  holder.detail.setOnClickListener(new OnClickListener(){
 		  @Override
 		  public void onClick(View v) {

 			  Intent detail_intent = null;
 			  detail_intent = new Intent(context, FullNews.class);
 			  startActivity(detail_intent);

 		  }
 	  });

 	 holder.event1.setOnClickListener(new OnClickListener(){
		  @Override
		  public void onClick(View v) {

			  Intent event1_intent = null;
			  event1_intent = new Intent(context, EventlineNews.class);
			  startActivity(event1_intent);

		  }
	  });
 	 holder.event2.setOnClickListener(new OnClickListener(){
		  @Override
		  public void onClick(View v) {

			  Intent event2_intent = null;
			  event2_intent = new Intent(context, EventlineNews.class);
			  startActivity(event2_intent);

		  }
	  });
 	 holder.event3.setOnClickListener(new OnClickListener(){
		  @Override
		  public void onClick(View v) {

			  Intent event3_intent = null;
			  event3_intent = new Intent(context, EventlineNews.class);
			  startActivity(event3_intent);

		  }
	  });
 	

 	  return v;   
 	  } 
    	}
        ///////////////////////////////////////////////////////////////////////listview adapter

        	@Override
        	public void onItemClick(AdapterView<?> parent, View view, int position,
              long id) {
		        ListView listview = (ListView) parent;
		        View toolbar= view.findViewById(R.id.toolbar);
		        NewsViewHolder holder = new NewsViewHolder();
		        JsonNewsItem p = adapter.getItem(position);
		        holder.image = (ImageView) view.findViewById(R.id.news_image);
		        plv.setSelectionFromTop(position, 0);
		        if(p.isOpen){
		        p.isOpen=false ; 
		        }
		        
		        else {
			        p.isOpen=true;
			        URL myFileUrl = null;   
	                if(p.getImage() != "null"){
	                try{   
	                myFileUrl = new URL(p.getImage());  
                       
                }   
                catch(MalformedURLException e)
                {   
                // Todo Auto-generated catch block   
                e.printStackTrace();   
                }   
	                try  
	                {   
		                    
		                HttpURLConnection conn = (HttpURLConnection)myFileUrl.openConnection();   
		                conn.setDoInput(true);   
		                conn.connect();   
		                int length = conn.getContentLength();
		                InputStream is = conn.getInputStream(); 
		                       
		                bmImg = BitmapFactory.decodeStream(is);
		                   
		                p.setbmImg(resizeBitmapImageFn(bmImg,500));
		                holder.image.setImageBitmap(resizeBitmapImageFn(bmImg,500));  
		                    
		            }   
		            catch(IOException e) 
	                {   
		            	e.printStackTrace();    
	                }
                }
        }
    
        holder = new NewsViewHolder();
        holder.keyword= (TextView) view.findViewById(R.id.dataItem01);
        holder.article= (TextView) view.findViewById(R.id.dataItem02);
        holder.event1 = (TextView) view.findViewById(R.id.event1);
        holder.event2 = (TextView) view.findViewById(R.id.event2);
        holder.event3 = (TextView) view.findViewById(R.id.event3);
        holder.image = (ImageView) view.findViewById(R.id.news_image);
        holder.detail = (TextView) view.findViewById(R.id.detail);
        holder.isOpen=p.getisOpen();
    
        view.setTag(holder);
    
    
        // Creating the expand animation for the item
        	ExpandAnimation expandAni = new ExpandAnimation(toolbar, 500);
        	
        	toolbar.startAnimation(expandAni);
        	listview.smoothScrollToPositionFromTop(position, 0);
        // Start the animation on the toolbar
        	
        	}
    
        	public Bitmap resizeBitmapImageFn(
        			Bitmap bmpSource, int maxResolution){ 
        		int iWidth = bmpSource.getWidth();      //비트맵이미지의 넓이
        		int iHeight = bmpSource.getHeight();     //비트맵이미지의 높이
        		int newWidth = iWidth ;
        		int newHeight = iHeight ;
        		float rate = 0.0f;
    //이미지의 가로 세로 비율에 맞게 조절
        		if(iWidth > iHeight ){
        			if(maxResolution < iWidth ){ 
        				rate = maxResolution / (float) iWidth ; 
        				newHeight = (int) (iHeight * rate); 
        				newWidth = maxResolution; 
        			}
        		}else{
        			if(maxResolution < iHeight ){
        				rate = maxResolution / (float) iHeight ; 
        				newWidth = (int) (iWidth * rate);
        				newHeight = maxResolution;
        			}
        		}
    
        		return Bitmap.createScaledBitmap(
        				bmpSource, newWidth, newHeight, true); 
        	}
        	
        	
    
}
