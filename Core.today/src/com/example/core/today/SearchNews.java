package com.example.core.today;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import android.annotation.TargetApi;
import android.os.Build;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

public class SearchNews extends Activity implements
										AdapterView.OnItemClickListener, StickyListHeadersListView.OnHeaderClickListener,
										StickyListHeadersListView.OnStickyHeaderOffsetChangedListener,
										StickyListHeadersListView.OnStickyHeaderChangedListener {
	
	
	private String pager = null;
	private StickyListHeadersListView stickyList;
	private SearchNewsAdapter mAdapter;
	private String query;
	
    private boolean fadeHeader = true;

 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_news);
		
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "THEjunggt120.otf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("PRISM");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_HOME | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
		
		Intent intent = getIntent();
		query = intent.getExtras().getString("query_value");
		
		pager = SearchQueryMaker(query);
		mAdapter = new SearchNewsAdapter(this, 0, pager);//RE
		
		TextView searchTitle = (TextView)findViewById(R.id.search_title);
		searchTitle.setText(query);
		
		stickyList = (StickyListHeadersListView) findViewById(R.id.search_listview);
        stickyList.setOnItemClickListener(this);
        stickyList.setOnHeaderClickListener(this);
        stickyList.setOnStickyHeaderChangedListener(this);
        stickyList.setOnStickyHeaderOffsetChangedListener(this);
        stickyList.addHeaderView(getLayoutInflater().inflate(R.layout.list_header, null));
        stickyList.setEmptyView(findViewById(R.id.empty));
        stickyList.setDrawingListUnderStickyHeader(true);
        stickyList.setAreHeadersSticky(true);
        stickyList.setAdapter(mAdapter);
		
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeButtonEnabled(true);
        
        stickyList.setStickyHeaderTopOffset(-20);

	}
	@Override
	public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
		/*try {
			JSONArray  ForUrl = null;
			JSONObject ForUrl2= null;
			ForUrl = new JSONArray(pager);
			ForUrl2= new JSONObject(ForUrl.getString(position));
			String sub = ForUrl2.getString("sub");
			ForUrl = new JSONArray(sub);
			ForUrl2 = new JSONObject(ForUrl.getString(0));
			
			String url = ForUrl2.getString("url");
			Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
			startActivity(intent);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(getBaseContext(), e.toString(), 5000).show();
		}*/
		   Toast.makeText(this, "Item " + position + " clicked!", Toast.LENGTH_SHORT).show();
	}
	
	@Override
    public void onHeaderClick(StickyListHeadersListView l, View header, int itemPosition, long headerId, boolean currentlySticky) {
        Toast.makeText(this, "Header " + headerId + " currentlySticky ? " + currentlySticky, Toast.LENGTH_SHORT).show();
    }
	
	@Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderOffsetChanged(StickyListHeadersListView l, View header, int offset) {
        if (fadeHeader && Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            header.setAlpha(1 - (offset / (float) header.getMeasuredHeight()));
        }
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onStickyHeaderChanged(StickyListHeadersListView l, View header, int itemPosition, long headerId) {
        header.setAlpha(1);
    }
    
	private String SearchQueryMaker(String query){

	    String line;
	    String page ="";
		String group1 = "http://kr.core.today/json/?d=20141108&n=20";
	    String sample = group1; // + query �� �� �� ��.
	    
	        URL url;
	        HttpURLConnection urlConnection;
	        BufferedReader bufreader;
	        try {
			url = new URL(sample);		
			urlConnection = (HttpURLConnection) url.openConnection();
			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			while((line=bufreader.readLine())!=null){
				page+=line;
			}

			urlConnection.disconnect();
			
			}
			catch(Exception e){

			}

	        return page;
	    }
	/*
	private String CategoryMaker(){

	    String line;
	    String page ="";
		String group1 = "http://kr.core.today/json2/?n=10&sl=1&s=-1&c="+category;
	    String sample = group1; 
	    Toast.makeText(getBaseContext(), sample, 5000).show();
	        URL url;
	        HttpURLConnection urlConnection;
	        BufferedReader bufreader;
	        try {
			url = new URL(sample);		

			urlConnection = (HttpURLConnection) url.openConnection();
			Toast.makeText(getBaseContext(), "111111111111111", 10000).show();
			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			Toast.makeText(getBaseContext(), "222222222222222", 10000).show();
			while((line=bufreader.readLine())!=null){
				page+=line;
			}
			Toast.makeText(getBaseContext(), page, 6000).show();
			urlConnection.disconnect();
			
			}
			catch(Exception e){
				Toast.makeText(getBaseContext(), e.toString(), 3000).show();
			}

	        return page;
	    }
	    */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	
	

}
