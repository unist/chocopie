package com.example.core.today;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class EventlineNews extends Activity{
	
	
	private String pager = null;
	private ListView mListView;
	private SearchNewsAdapter mAdapter;
	private String query;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eventline_news);
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "THEjunggt120.otf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("PRISM");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_HOME | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////\
		
		pager = SearchQueryMaker(query);
		mAdapter = new SearchNewsAdapter(this, 0, pager);//RE
		

		TextView searchTitle = (TextView)findViewById(R.id.eventline_title);
		searchTitle.setText(query);
		
		mListView = (ListView)findViewById(R.id.eventline_listview);
		mListView.setAdapter(mAdapter);
		mListView.setOnItemClickListener(onClickListItem);
		
		
		
	}
	
	private OnItemClickListener onClickListItem = new OnItemClickListener(){
		
		@Override
		public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id) {
			try {
				JSONArray  ForUrl = null;
				JSONObject ForUrl2= null;
				ForUrl = new JSONArray(pager);
				ForUrl2= new JSONObject(ForUrl.getString(position));
				String sub = ForUrl2.getString("sub");
				ForUrl = new JSONArray(sub);
				ForUrl2 = new JSONObject(ForUrl.getString(0));
				
				String url = ForUrl2.getString("url");
				Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
				startActivity(intent);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toast.makeText(getBaseContext(), e.toString(), 5000).show();
			}
		}
		
	};
	
	private String SearchQueryMaker(String query){

	    String line;
	    String page ="";
		String group1 = "http://kr.core.today/json/?d=20141108&n=10";
	    String sample = group1; // + query �� �� �� ��.
	    
	        URL url;
	        HttpURLConnection urlConnection;
	        BufferedReader bufreader;
	        try {
			url = new URL(sample);		
			urlConnection = (HttpURLConnection) url.openConnection();
			bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			while((line=bufreader.readLine())!=null){
				page+=line;
			}

			urlConnection.disconnect();
			
			}
			catch(Exception e){

			}

	        return page;
	    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}

}
