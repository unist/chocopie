package com.example.core.today;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.BaseAdapter;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class CategoryItem extends Activity{
	
	String pager = null;
	CategoryAdapter gridViewCustomAdapter;
	private String category;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.category_news);
		Intent intent = getIntent();
		int id = intent.getIntExtra("category", 0);
		
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "THEjunggt120.otf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("PRISM");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_HOME | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
		Intent receivedintent = getIntent();
		int get_id = receivedintent.getExtras().getInt("category");
	
		if(get_id == R.id.ctg_economy)
			category = "경제";
		else if(get_id == R.id.ctg_entertain)
			category = "연예";
		else if(get_id == R.id.ctg_global)
			category = "\uae00\ub85c\ubc8c";
		else if(get_id == R.id.ctg_it)
			category = "IT";
		else if(get_id == R.id.ctg_main)
			category = "주요뉴스";
		else if(get_id == R.id.ctg_politics)
			category = "정치사회";
		else if(get_id == R.id.ctg_sports)
			category = "스포츠";
		GridView gridView = (GridView)findViewById(R.id.gridView);
		
		
		TextView text = (TextView)findViewById(R.id.ctg);
		text.setText(category);
		
		
		Button btn = (Button)findViewById(R.id.finish);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		pager= CategoryMaker();
		gridViewCustomAdapter = new CategoryAdapter(this, get_id, pager);
		gridView.setAdapter(gridViewCustomAdapter);
		
		
		gridView.setOnItemClickListener(new OnItemClickListener() {
		
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				try {
					JSONArray  ForUrl = null;
					JSONObject ForUrl2= null;
					ForUrl = new JSONArray(pager);
					ForUrl2= new JSONObject(ForUrl.getString(position));
					String sub = ForUrl2.getString("sub");
					ForUrl = new JSONArray(sub);
					ForUrl2 = new JSONObject(ForUrl.getString(0));
					
					String url = ForUrl2.getString("url");
					Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(url));
					startActivity(intent);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					Toast.makeText(getBaseContext(), e.toString(), 5000).show();
				}
				
				
			}
			
		});

	}
	

	private String CategoryMaker(){

    String line;
    String page ="";
	String group1 = "http://kr.core.today/json2/?n=10&sl=1&s=-1&c="+category;
    String sample = group1; 
    Toast.makeText(getBaseContext(), sample, 5000).show();
        URL url;
        HttpURLConnection urlConnection;
        BufferedReader bufreader;
        try {
		url = new URL(sample);		

		urlConnection = (HttpURLConnection) url.openConnection();
		Toast.makeText(getBaseContext(), "111111111111111", 10000).show();
		bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
		Toast.makeText(getBaseContext(), "222222222222222", 10000).show();
		while((line=bufreader.readLine())!=null){
			page+=line;
		}
		Toast.makeText(getBaseContext(), page, 6000).show();
		urlConnection.disconnect();
		
		}
		catch(Exception e){
			Toast.makeText(getBaseContext(), e.toString(), 3000).show();
		}

        return page;
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}
