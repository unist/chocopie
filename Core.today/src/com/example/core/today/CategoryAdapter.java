package com.example.core.today;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CategoryAdapter extends ArrayAdapter{

	String jsonfile;
	
	public CategoryAdapter(Context context, int resource, String json) {
		super(context, resource);
		mcontext = context;
		id = resource;
		jsonfile = json;

	}
	int id;
	Context mcontext;
	
	
	public int getCount(){
		return 10;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		View row = convertView;
		
		if(row==null && position <= getCount()){
			LayoutInflater inflater = ((Activity) mcontext).getLayoutInflater();
            row = inflater.inflate(R.layout.grid_row, parent, false);
                        
           TextView textViewTitle = (TextView) row.findViewById(R.id.title_text);
           ImageView newsimage = (ImageView) row.findViewById(R.id.image);
         
           
           JSONObject test = null;          
           try {
        	   
			JSONArray alljson = new JSONArray(jsonfile);
			
			test = new JSONObject(alljson.getString(position));
			
			Toast.makeText(mcontext, test.getString("k"), 6000).show();
			if(test.getString("i") != "null"){
			
				URL articleURL = null;
				articleURL = new URL(test.getString("i"));
			
				HttpURLConnection conn = (HttpURLConnection)articleURL.openConnection();   
				conn.setDoInput(true);   
				conn.connect();   
				InputStream is = conn.getInputStream(); 
				Bitmap bmImg; 
				bmImg = BitmapFactory.decodeStream(is);
           
				bmImg = resizeBitmapImageFn(bmImg);
				conn.disconnect();
				newsimage.setImageBitmap(bmImg);
			}
			textViewTitle.setText(test.getString("k"));
			Toast.makeText(mcontext, "555555555555555", 4000).show();
           } 
           catch (Exception e) {
			// TODO Auto-generated catch block
        	//Toast.makeText(mcontext, e.toString(), 4000).show();
           }
                
		}		
	
		return row;
	}
	public Bitmap resizeBitmapImageFn(
    		Bitmap bmpSource){    		        
    		        return Bitmap.createScaledBitmap(
    		bmpSource, 50, 50, true); 
    		}
}
